package com.example.sipoltas_petugas;


import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.audiofx.EnvironmentalReverb;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.*;
import android.support.v4.BuildConfig;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.Api;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import id.zelory.compressor.Compressor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Url;


/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends Fragment {


    public EditProfileFragment() {
        // Required empty public constructor
    }

    final static int CAMERA = 1;
    final static int GALLERY = 2;
    final static int CROP = 3;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static final String MyPREFERENCES = "MyPrefs";

    TextView textEmail;
    EditText editNama,editAlamat,editPassword;
    Spinner province,city,subdistric;
    ImageView photoProfile;
    String urlRequestGet,urlRequestPost,urlRequestGetProvince,urlRequestGetCity,urlRequestGetSubdistrict,email;

    List<Integer> idProvince = new ArrayList<>();
    List<Integer> idCity = new ArrayList<>();
    List<Integer> idSubDistric = new ArrayList<>();
    List<String> namaProvince = new ArrayList<>();
    List<String> namaCity = new ArrayList<>();
    List<String> namaSubDistrict = new ArrayList<>();

    int idProvicePost,idCityPost,idSubdistrictPos;

    String device_token_id;

    String namaInput,addressInput;

    String baseUrl = ApiClient.BASE_URL;

    Uri uri;
    File file,compressedFile;

    int provinsi = 0;
    int kota = 0;
    int kecamatan = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_edit_profile,container,false);

        Boolean statusInternet = Connection.isConnected(getActivity());

        if(!statusInternet){
            Toast.makeText(getActivity(), "You need a internet connection", Toast.LENGTH_LONG).show();
            return view;
        }

//        Bundle bundle = this.getArguments();
//        if(bundle!=null){
//            device_token_id = bundle.getString("device_token_petugas");
//        }

//        Intent intent = getActivity().getIntent();
//        device_token_id = intent.getExtras().getString("device_token_petugas");
        device_token_id = getToken();

        editNama = (EditText) view.findViewById(R.id.name);
        editAlamat = (EditText) view.findViewById(R.id.address);
        editPassword = (EditText) view.findViewById(R.id.password);

        textEmail = (TextView) view.findViewById(R.id.email);

        province = (Spinner) view.findViewById(R.id.provinsi);
        city =(Spinner) view.findViewById(R.id.kota);
        subdistric = (Spinner) view.findViewById(R.id.kecamatan);

        urlRequestGet = baseUrl+"v1/user/get?token="+device_token_id;
        urlRequestPost = baseUrl+"v1/user/update?token="+device_token_id;
        urlRequestGetProvince = baseUrl+"v1/user/region";


        getProfile(urlRequestGet);
        getProvince();

        photoProfile = (ImageView) view.findViewById(R.id.photoprofile);
        photoProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });

        Button saveButton = (Button) view.findViewById(R.id.savebutton);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                namaInput = editNama.getText().toString();
                addressInput = editAlamat.getText().toString();
                cekKosong();
            }
        });
        return view;
    }

    private void selectImage(){
        final CharSequence[] photoSelection = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Add photo!");
        alertDialog.setItems(photoSelection, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                if(photoSelection[position].equals("Take Photo")){
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    file = new File(Environment.getExternalStorageDirectory(),"file"+String.valueOf(System.currentTimeMillis()+".jpg"));
                    uri = Uri.fromFile(file);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
                    cameraIntent.putExtra("return_data",true);
                    startActivityForResult(cameraIntent,CAMERA);
                } else if(photoSelection[position].equals("Choose from Gallery")){
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent,GALLERY);
                }
            }
        });
        alertDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==getActivity().RESULT_CANCELED && resultCode==getActivity().RESULT_OK) {
            return;
        } else if(requestCode==CAMERA){
            //Bitmap bitmap = (Bitmap) data.getExtras().get("data");

            cropImage();

            //photoProfile.setImageBitmap(bitmap);
        } else if(requestCode==GALLERY){
            if(data!=null){
                uri = data.getData();
                //Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);

                cropImage();
                //photoProfile.setImageBitmap(bitmap);
            }
        } else if(requestCode==CROP){
            if(data != null){
                Uri photoUri = data.getData();
                Bitmap bitmap = decoderUriAsBitmap(photoUri);

                photoProfile.setImageBitmap(bitmap);
            }
        }
    }

    private Bitmap decoderUriAsBitmap(Uri mUri){
        Bitmap bitmap = null;
        try{
            bitmap = BitmapFactory.decodeStream(getActivity().getContentResolver().openInputStream(mUri));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
        return bitmap;
    }

    private void cropImage(){
        try {
            Intent CropIntent = new Intent("com.android.camera.action.CROP");
            CropIntent.setDataAndType(uri, "image/*");

            CropIntent.putExtra("crop", "true");
            CropIntent.putExtra("outputX", 150);
            CropIntent.putExtra("outputY", 150);
            CropIntent.putExtra("aspectX", 1);
            CropIntent.putExtra("aspectY", 1);
            CropIntent.putExtra("scaleUp", true);
            CropIntent.putExtra("return_data", true);

            startActivityForResult(CropIntent, CROP);
        } catch (ActivityNotFoundException notfound){

        }
    }

    public String getToken(){
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES,getActivity().MODE_PRIVATE);
        String token = sharedPreferences.getString("token","0");
        if (token.equals("0")){
            startActivity(new Intent(getActivity(),LoginActivity.class));
            getActivity().finish();
        }
        return token;
    }


    private void getProvince(){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelProvince> call = apiInterfaces.MODEL_PROVINCE_CALL(urlRequestGetProvince);
        call.enqueue(new Callback<ModelProvince>() {
            @Override
            public void onResponse(Call<ModelProvince> call, Response<ModelProvince> response) {
                int code = response.code();
                ModelProvince modelProvince = response.body();
                if(code == 200){
                    String status = modelProvince.getStatus();
                    if(status.equals("success")){
                        idProvince.clear();
                        namaProvince.clear();

                        idProvince.add(0,0);
                        namaProvince.add(0,"--Pilih Provinsi--");

                        int nProvince = modelProvince.getResult().size();
                        for(int i = 0; i<nProvince; i++){
                            int id = modelProvince.getResult().get(i).getId();
                            String nama = modelProvince.getResult().get(i).getName();

                            idProvince.add(i+1,id);
                            namaProvince.add(i+1,nama);
                        }
                        setSpinnerProvince();
                        setSelectionProvince(provinsi);
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelProvince> call, Throwable t) {

            }
        });
    }

    private void getCity(){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelProvince> call = apiInterfaces.MODEL_PROVINCE_CALL(urlRequestGetCity);
        call.enqueue(new Callback<ModelProvince>() {
            @Override
            public void onResponse(Call<ModelProvince> call, Response<ModelProvince> response) {
                int code = response.code();
                ModelProvince modelProvince = response.body();
                if(code == 200){
                    String status = modelProvince.getStatus();
                    if(status.equals("success")){
                        idCity.clear();
                        namaCity.clear();

                        idCity.add(0,0);
                        namaCity.add(0,"--Pilih Kota--");

                        int nCity = modelProvince.getResult().size();
                        for(int i = 0; i<nCity; i++){
                            int id = modelProvince.getResult().get(i).getId();
                            String nama = modelProvince.getResult().get(i).getName();

                            idCity.add(i+1,id);
                            namaCity.add(i+1,nama);
                        }
                        setSpinnerCity();
                        setSelectionCity(kota);
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelProvince> call, Throwable t) {

            }
        });
    }

    private void getSubdistric(){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelProvince> call = apiInterfaces.MODEL_PROVINCE_CALL(urlRequestGetSubdistrict);
        call.enqueue(new Callback<ModelProvince>() {
            @Override
            public void onResponse(Call<ModelProvince> call, Response<ModelProvince> response) {
                int code = response.code();
                ModelProvince modelProvince = response.body();
                if(code == 200){
                    String status = modelProvince.getStatus();
                    if(status.equals("success")){
                        idSubDistric.clear();
                        namaSubDistrict.clear();

                        idSubDistric.add(0,0);
                        namaSubDistrict.add(0,"--Pilih Kecamatan--");

                        int nSubdistrict = modelProvince.getResult().size();
                        for(int i=0; i<nSubdistrict; i++){
                            int id = modelProvince.getResult().get(i).getId();
                            String nama = modelProvince.getResult().get(i).getName();

                            idSubDistric.add(i+1,id);
                            namaSubDistrict.add(i+1,nama);
                        }
                        setSpinnerSubdistrict();
                        setSelectionSubdistrict(kecamatan);
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelProvince> call, Throwable t) {

            }
        });
    }

    private void setSpinnerProvince(){
        ArrayAdapter<String> adapterProvince = new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,namaProvince);
        province.setAdapter(adapterProvince);
        province.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                urlRequestGetCity = baseUrl+"v1/user/region/city/"+idProvince.get(position);
                idProvicePost = idProvince.get(position);
                getCity();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSpinnerCity(){
        ArrayAdapter<String> adapterCity = new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,namaCity);
        city.setAdapter(adapterCity);
        city.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                urlRequestGetSubdistrict = baseUrl+"v1/user/region/sub/"+idCity.get(position);
                idCityPost = idCity.get(position);
                getSubdistric();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setSpinnerSubdistrict(){
        ArrayAdapter<String> adapterSubdistrict = new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,namaSubDistrict);
        subdistric.setAdapter(adapterSubdistrict);
        subdistric.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                idSubdistrictPos = idSubDistric.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void cekKosong(){
        if(namaInput==null){
            Toast.makeText(getActivity(),"Isikan Nama Anda",Toast.LENGTH_SHORT).show();
            return;
        }
        else if(addressInput==null){
            Toast.makeText(getActivity(),"Isikan Alamat Anda",Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            UpdateProfile(urlRequestPost,namaInput,addressInput);
        }
    }

    private void getProfile(final String urlRequest){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelProfile> call = apiInterfaces.MODEL_CALL(urlRequest);
        call.enqueue(new Callback<ModelProfile>() {
            @Override
            public void onResponse(Call<ModelProfile> call, Response<ModelProfile> response) {
                int code = response.code();
                ModelProfile modelProfile = response.body();
                if(code==200){
                    String namaPetugas = modelProfile.getResult().getFullName();
                    String address = (String) modelProfile.getResult().getAddress();
                    provinsi = modelProfile.getResult().getProvinceId();
                    kota = modelProfile.getResult().getCityId();
                    kecamatan = modelProfile.getResult().getSubdistricId();
                    email = modelProfile.getResult().getEmail();

//                    Toast.makeText(getActivity(),"Berhasil",Toast.LENGTH_SHORT).show();

                    editNama.setText(namaPetugas);
                    editAlamat.setText(address);
//                    textProvinsi.setText(String.valueOf(provinsi));
//                    textKota.setText(String.valueOf(kota));
//                    textKecamatan.setText(String.valueOf(kecamatan));
                    textEmail.setText(email);
                } else {
                    Toast.makeText(getActivity(),"Tidak Bisa Mengupdate Data",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(),LoginActivity.class));
                }
            }

            @Override
            public void onFailure(Call<ModelProfile> call, Throwable t) {
                Toast.makeText(getActivity(),"Tidak Bisa Mengupdate Data",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(),LoginActivity.class));
            }
        });
    }

    private void setSelectionProvince(int idProvinsi) {
        for (int i = 0; i < idProvince.size(); i++) {
            if (idProvince.get(i) == idProvinsi) {
                province.setSelection(i);
                urlRequestGetCity = baseUrl + "v1/user/region/city/" + idProvinsi;
                getCity();
                break;
            }
        }
    }

    private void setSelectionCity(int idKota) {
         for (int i = 0; i < idCity.size(); i++) {
             if (idCity.get(i) == idKota) {
                 city.setSelection(i);
                 urlRequestGetSubdistrict = baseUrl + "v1/user/region/sub/" + idKota;
                 getSubdistric();
                 break;
             }
         }
    }

    private void setSelectionSubdistrict(int idKecamatan){
        for(int i=0;i<idSubDistric.size();i++){
            if(idSubDistric.get(i)== idKecamatan){
                subdistric.setSelection(i);
                break;
            }
        }
    }

    private void UpdateProfile(final String urlRequest,final String nama,final String address){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelEditProfile> call = apiInterfaces.MODEL_PROFILE_CALL(urlRequest,email,nama,address,idProvicePost,idCityPost,idSubdistrictPos);
        call.enqueue(new Callback<ModelEditProfile>() {
            @Override
            public void onResponse(Call<ModelEditProfile> call, Response<ModelEditProfile> response) {
                int code = response.code();
                ModelEditProfile modelEditProfile = response.body();
                if(code==200){
                    String statusEdit = modelEditProfile.getStatus();
                    if(statusEdit.equals("success")){
                        Intent intent = new Intent(getActivity(),MainActivity.class);
                        intent.putExtra("device_token_petugas",device_token_id);
                        startActivity(intent);
                        getActivity().finish();
                    }
                    else if(statusEdit.equals("error")){
                        Toast.makeText(getActivity(), "Your email or password incorrect!", Toast.LENGTH_SHORT).show();
                    }
                }
                else if(code == 401) {
                    Toast.makeText(getActivity(), "Your email or password incorrect!", Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(),LoginActivity.class));
                    //Toast.makeText(LoginActivity.this, statusLogin, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(LoginActivity.this, messageLogin, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ModelEditProfile> call, Throwable t) {
                Toast.makeText(getActivity(), "Can't edit your profile", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
