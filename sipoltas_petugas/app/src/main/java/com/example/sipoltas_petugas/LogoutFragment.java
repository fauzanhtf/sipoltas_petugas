package com.example.sipoltas_petugas;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class LogoutFragment extends Fragment {


    public LogoutFragment() {
        // Required empty public constructor
    }

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static final String MyPREFERENCES = "MyPrefs";

    String baseUrl = ApiClient.BASE_URL;
    String urlRequestPost;
    String device_token_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_logout,container,false);

        Boolean statusInternet = Connection.isConnected(getActivity());

        if(!statusInternet){
            Toast.makeText(getActivity(), "You need a internet connection", Toast.LENGTH_LONG).show();
            return view;
        }

        urlRequestPost = baseUrl+"v1/user/logout?token="+device_token_id;

        editor = getActivity().getSharedPreferences(MyPREFERENCES, getActivity().MODE_PRIVATE).edit();

        device_token_id = getToken();

        Logout();

        return view;
    }

    private void Logout(){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelLogout> call = apiInterfaces.MODEL_LOGOUT_CALL(urlRequestPost,device_token_id);
        call.enqueue(new Callback<ModelLogout>() {
            @Override
            public void onResponse(Call<ModelLogout> call, Response<ModelLogout> response) {
                int code = response.code();
                ModelLogout modelLogout = response.body();
                if(code == 200){
                    String status = modelLogout.getStatus();
                    if(status.equals("success")){
                        editor.putBoolean("isLogin",false);
                        editor.putString("token","0");
                        startActivity(new Intent(getActivity(),LoginActivity.class));
                        getActivity().finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelLogout> call, Throwable t) {

            }
        });
    }

    public String getToken(){
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES,getActivity().MODE_PRIVATE);
        String token = sharedPreferences.getString("token","0");
        if (token.equals("0")){
            startActivity(new Intent(getActivity(),LoginActivity.class));
            getActivity().finish();
        }
        return token;
    }

}
