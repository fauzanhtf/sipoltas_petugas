package com.example.sipoltas_petugas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Fauzan on 26/07/2017.
 */

public class ResultLogin {
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("bp_user_id")
    @Expose
    private Integer bpUserId;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Integer getBpUserId() {
        return bpUserId;
    }

    public void setBpUserId(Integer bpUserId) {
        this.bpUserId = bpUserId;
    }
}
