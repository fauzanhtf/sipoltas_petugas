package com.example.sipoltas_petugas;

//import android.app.Fragment;
//import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String baseUrl = ApiClient.BASE_URL;
    String device_token_id;

    String idLaporan;

    TextView namaProfile;
    View navHeader;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Boolean statusInternet = Connection.isConnected(this);

        if(!statusInternet){
            Toast.makeText(this, "You need a internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        String from;

        Intent intent = getIntent();
        if(intent.getExtras().getString("From")!=null){
            from = intent.getExtras().getString("From");
        } else {
            from = "splash";
        }
//        device_token_id = intent.getExtras().getString("device_token_petugas");

        device_token_id = getToken();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        String urlRequestGet = baseUrl+"v1/user/get?token="+device_token_id;

        getProfile(urlRequestGet);

        navHeader = navigationView.getHeaderView(0);
        namaProfile = (TextView) navHeader.findViewById(R.id.namaPetugas);

        //device_token_id = getToken();
        if(from.equals("splash")){
            selectedScreen(R.id.profile);
        } else if(from.equals("notifikasi")) {
            idLaporan = intent.getExtras().getString("IdLaporan");
            selectedScreen(10);
        }

    }

    private void getProfile(final String urlRequest){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelProfile> call = apiInterfaces.MODEL_CALL(urlRequest);
        call.enqueue(new Callback<ModelProfile>() {
            @Override
            public void onResponse(Call<ModelProfile> call, Response<ModelProfile> response) {
                int code = response.code();
                ModelProfile modelProfile = response.body();
                if(code==200){
                    String namaPetugas = modelProfile.getResult().getFullName();

                    namaProfile.setText(namaPetugas);
                    //Toast.makeText(ProfileActivity.this,"Berhasil",Toast.LENGTH_SHORT).show();

                } else {
                    Toast.makeText(MainActivity.this,"Gagal",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(MainActivity.this,LoginActivity.class));
                }
            }

            @Override
            public void onFailure(Call<ModelProfile> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Gagal",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainActivity.this,LoginActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        selectedScreen(item.getItemId());
        return true;
    }

    private void selectedScreen(int idItem){

        Fragment fragment =null;

        switch (idItem){
            case R.id.pengaduan:
                fragment = new ListLaporanFragment() ;
                getSupportActionBar().setTitle("Pengaduan");
                break;
            case R.id.updatePosition:
                fragment = new UpdatePositionFragment();
                getSupportActionBar().setTitle("Update Position");
                break;
            case R.id.posPenjagaan:
                fragment = new UpdatePosFragment();
                getSupportActionBar().setTitle("Update Pos Penjagaan");
                break;
            case R.id.logout:
                fragment = new LogoutFragment();
                break;
            case R.id.profile:
                fragment = new ProfileFragment() ;
                getSupportActionBar().setTitle("Profile");
                break;
            case R.id.editProfile:
                fragment = new EditProfileFragment();
                getSupportActionBar().setTitle("Edit Profile");
                break;
            case 10:
                fragment = new LaporanMasukFragment();
                getSupportActionBar().setTitle("Laporan Masuk");
                Bundle bundle = new Bundle();
                bundle.putString("idLaporan",idLaporan);
                fragment.setArguments(bundle);
                break;
        }

        if(fragment!=null){
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.fragment_view,fragment);
            fragmentTransaction.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    public String getToken(){
        sharedPreferences = getSharedPreferences(MyPREFERENCES,MODE_PRIVATE);
        String token = sharedPreferences.getString("token","0");
        if (token.equals("0")){
            startActivity(new Intent(MainActivity.this,LoginActivity.class));
            finish();
        }
        return token;
    }
}
