package com.example.sipoltas_petugas;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import id.zelory.compressor.Compressor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class PerbaruiLaporanFragment extends Fragment {


    public PerbaruiLaporanFragment() {
        // Required empty public constructor
    }

    final static int CAMERA = 1;
    final static int GALLERY = 2;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static final String MyPREFERENCES = "MyPrefs";

    TextView waktuLaporan;
    EditText name,phone,keterangan;
    ImageView photoKejadian;
    Spinner spinnerStatusKejadian;

    ArrayList<String> namaStatusKejadian = new ArrayList<>();
    ArrayList<Integer> idStatusKejadian = new ArrayList<>();

    String device_token_id;
    String idLaporan;

    String urlRequestGet,urlRequestStatusLaporan,urlRequestUpdate;

    String baseUrl = ApiClient.BASE_URL;

    String namaKorban, telpKorban, keteranganKejadian;
    int statusKejadian;

    Uri uri;
    File file,actualFile,compressedFile;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_perbarui_laporan, container, false);

        Boolean statusInternet = Connection.isConnected(getActivity());

        if(!statusInternet){
            Toast.makeText(getActivity(), "You need a internet connection", Toast.LENGTH_LONG).show();
            return view;
        }

        Bundle bundle = this.getArguments();
        if(bundle!=null){
            idLaporan = bundle.getString("idLaporan");
            //device_token_id = bundle.getString("token");
        }
        device_token_id = getToken();

        waktuLaporan = (TextView) view.findViewById(R.id.waktuPelaporan);

        name = (EditText) view.findViewById(R.id.namaKorban);
        phone = (EditText) view.findViewById(R.id.noTelpKorban);
        keterangan = (EditText) view.findViewById(R.id.keteranganKecelakaan);

        spinnerStatusKejadian = (Spinner) view.findViewById(R.id.statusKecelakaan);

        urlRequestGet = baseUrl+"v1/report/report/"+idLaporan+"?token="+device_token_id;
        urlRequestStatusLaporan = baseUrl+"v1/report/status?token="+device_token_id;
        urlRequestUpdate = baseUrl+"v1/report/update/"+idLaporan+"?token="+device_token_id;

        getStatusLaporan();
        getLaporan();

        Button uploadPhoto = (Button) view.findViewById(R.id.uploadFoto);
        uploadPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickPhoto();
            }
        });

        photoKejadian = (ImageView) view.findViewById(R.id.photoKejadian);

        Button updateLaporan = (Button) view.findViewById(R.id.perbaruiLaporanButton);
        updateLaporan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                namaKorban = name.getText().toString().trim();
                telpKorban = phone.getText().toString().trim();
                keteranganKejadian = keterangan.getText().toString().trim();
                cekKosong();
            }
        });

        return view;
    }

    public String getReadableFileSize(long size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    private void pickPhoto(){
        final CharSequence[] photoSelection = {"Take Photo", "Choose from Gallery"};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Add photo!");
        alertDialog.setItems(photoSelection, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int position) {
                if(photoSelection[position].equals("Take Photo")){
                    Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    file = new File(Environment.getExternalStorageDirectory(),"file"+String.valueOf(System.currentTimeMillis()+".jpg"));
                    uri = Uri.fromFile(file);
                    cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,uri);
                    cameraIntent.putExtra("return_data",true);
                    startActivityForResult(cameraIntent,CAMERA);
                } else if(photoSelection[position].equals("Choose from Gallery")){
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent,GALLERY);
                }
            }
        });
        alertDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==getActivity().RESULT_CANCELED && resultCode==getActivity().RESULT_OK) {
            return;
        } else if(requestCode==CAMERA){
//            Bitmap bitmap = (Bitmap) data.getExtras().get("data");
//            String path = MediaStore.Images.Media.insertImage(getActivity().getContentResolver(),bitmap,null,null);
//            Uri uriResult = Uri.parse(path);
            try {
                actualFile = FileUtil.from(getActivity(),uri);
                Toast.makeText(getActivity(),getReadableFileSize(actualFile.length()),Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
            CompressImage();
            //photoProfile.setImageBitmap(bitmap);
        } else if(requestCode==GALLERY){
            if(data!=null){
                uri = data.getData();
                try {
                    actualFile = FileUtil.from(getActivity(),data.getData());
                    Toast.makeText(getActivity(),getReadableFileSize(actualFile.length()),Toast.LENGTH_SHORT).show();
                    //Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                CompressImage();
                //photoProfile.setImageBitmap(bitmap);
            }
        }
    }

    private void CompressImage(){
        try {
            compressedFile = new Compressor(getActivity())
                    .setMaxWidth(480)
                    .setMaxHeight(320)
                    .setQuality(50)
                    .setCompressFormat(Bitmap.CompressFormat.WEBP)
                    .compressToFile(actualFile);

            photoKejadian.setImageBitmap(BitmapFactory.decodeFile(compressedFile.getAbsolutePath()));
            Toast.makeText(getActivity(),getReadableFileSize(compressedFile.length()),Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void cekKosong(){
        if(namaKorban.equals(null)){
            Toast.makeText(getActivity(),"Isi nama korban",Toast.LENGTH_SHORT).show();
            return;
        } else if(telpKorban.equals(null)){
            Toast.makeText(getActivity(),"Isi nomor telepon korban",Toast.LENGTH_SHORT).show();
            return;
        } else if(keteranganKejadian.equals(null)){
            Toast.makeText(getActivity(),"Isi keterangan tentang kejadian",Toast.LENGTH_SHORT).show();
            return;
        } else {
            updateLaporanMasuk();
        }
    }

    private void updateLaporanMasuk(){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelUpdateLaporan> call = apiInterfaces.MODEL_UPDATE_LAPORAN_CALL(urlRequestUpdate,namaKorban,telpKorban,keteranganKejadian,statusKejadian);
        call.enqueue(new Callback<ModelUpdateLaporan>() {
            @Override
            public void onResponse(Call<ModelUpdateLaporan> call, Response<ModelUpdateLaporan> response) {
                int code = response.code();
                ModelUpdateLaporan modelUpdateLaporan = response.body();
                if(code==200){
                    String status = modelUpdateLaporan.getStatus();
                    if(status.equals("success")){
                        Toast.makeText(getActivity(),"Laporan masuk berhasil di update",Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getActivity(),"Laporan gagal di update!, coba beberapa saat lagi",Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getActivity(),"Terjadi kesalahan saat mengupdate data laporan",Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ModelUpdateLaporan> call, Throwable t) {
                Toast.makeText(getActivity(),"Terjadi kesalahan saat mengupdate data laporan",Toast.LENGTH_SHORT).show();
            }
        });
    }

    public String getToken(){
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES,getActivity().MODE_PRIVATE);
        String token = sharedPreferences.getString("token","0");
        if (token.equals("0")){
            startActivity(new Intent(getActivity(),LoginActivity.class));
            getActivity().finish();
        }
        return token;
    }

    private void  getStatusLaporan(){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelListStatus> call = apiInterfaces.MODEL_LIST_STATUS_CALL(urlRequestStatusLaporan);
        call.enqueue(new Callback<ModelListStatus>() {
            @Override
            public void onResponse(Call<ModelListStatus> call, Response<ModelListStatus> response) {
                int code = response.code();
                ModelListStatus modelListStatus = response.body();
                if(code==200){
                    namaStatusKejadian.clear();
                    idStatusKejadian.clear();

                    namaStatusKejadian.add(0,"--Pilih Status Kejadian");
                    idStatusKejadian.add(0,0);
                    String status = modelListStatus.getStatus();
                    if(status.equals("success")){
                        int nList = modelListStatus.getResult().size();
                        for(int i = 0; i<nList; i++){
                            String statusName = modelListStatus.getResult().get(i).getName();
                            int statusId = modelListStatus.getResult().get(i).getId();

                            namaStatusKejadian.add(i+1,statusName);
                            idStatusKejadian.add(i+1,statusId);
                        }
                        setSpinnerStatus();
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelListStatus> call, Throwable t) {

            }
        });
    }

    private void setSpinnerStatus(){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,namaStatusKejadian);
        spinnerStatusKejadian.setAdapter(adapter);
        spinnerStatusKejadian.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statusKejadian = idStatusKejadian.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getLaporan(){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelLaporanMasuk> call = apiInterfaces.MODEL_LAPORAN_MASUK_CALL(urlRequestGet);
        call.enqueue(new Callback<ModelLaporanMasuk>() {
            @Override
            public void onResponse(Call<ModelLaporanMasuk> call, Response<ModelLaporanMasuk> response) {
                int code = response.code();
                ModelLaporanMasuk modelLaporanMasuk = response.body();
                if(code==200){
                    String getStatus = modelLaporanMasuk.getStatus();
                    if(getStatus.equals("success")){
                        String waktu = modelLaporanMasuk.getResult().getCreatedAt();

                        waktuLaporan.setText(waktu);
                    } else {

                    }
                } else {

                }
            }

            @Override
            public void onFailure(Call<ModelLaporanMasuk> call, Throwable t) {

            }
        });
    }

}
