package com.example.sipoltas_petugas;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Fauzan on 26/07/2017.
 */

public interface ApiInterfaces {
    //@GET("v1/user/1?device_token_id=fb6ca13e13af53ad69307c7f9a0cb23d568bfd46")
    @FormUrlEncoded
    @POST("v1/user/login")
    Call<ModelLogin> MODEL_CALL(@Field("email") String email, @Field("password") String password );

    //@FormUrlEncoded
    @GET
    Call<ModelProfile> MODEL_CALL(@Url String url);

    @FormUrlEncoded
    @POST
    Call<ModelEditProfile> MODEL_PROFILE_CALL(@Url String url, @Field("email") String email,@Field("full_name") String nama, @Field("address") String address, @Field("province_id") int province_id, @Field("city_id") int city_id, @Field("subdistric_id") int subdistric_id);

    @FormUrlEncoded
    @POST
    Call<ModelUpdatePosition> MODEL_UPDATE_POSITION_CALL(@Url String url, @Field("latitude") String latitude, @Field("longitude") String longitude);

    @GET
    Call<ModelLaporanMasuk> MODEL_LAPORAN_MASUK_CALL(@Url String url);

    @GET
    Call<ModelUpdatePos> MODEL_UPDATE_POS_CALL(@Url String url);

    @GET
    Call<ModelJumlahPetugas> MODEL_JUMLAH_PETUGAS_CALL(@Url String url);

    @FormUrlEncoded
    @POST
    Call<ModelUpdatePosition> MODEL_UPDATE_POS_CALL(@Url String url, @Field("latitude") String latitude, @Field("longitude") String longitude, @Field("bp_office_id") int bp_office);

    @GET
    Call<ModelProvince> MODEL_PROVINCE_CALL(@Url String url);

    @GET
    Call<ModelListLaporanMasuk> MODEL_LIST_LAPORAN_MASUK_CALL(@Url String url);

    @GET
    Call<ModelListStatus> MODEL_LIST_STATUS_CALL(@Url String url);

    @FormUrlEncoded
    @POST
    Call<ModelLogout> MODEL_LOGOUT_CALL(@Url String url, @Field("token") String token );

    @FormUrlEncoded
    @POST
    Call<ModelUpdateLaporan> MODEL_UPDATE_LAPORAN_CALL(@Url String url, @Field("name") String name, @Field("phone") String phone, @Field("notes") String noted, @Field("status_id") int status);

    @FormUrlEncoded
    @POST
    Call<ModelUpdateLaporan> MODEL_UPDATE_OFFICER_ID_CALL(@Url String url, @Field("officer_id") int officer_id);
}
