package com.example.sipoltas_petugas;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends Activity {

    String email,password;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Boolean statusInternet = Connection.isConnected(this);

        if(!statusInternet){
            Toast.makeText(this, "You need a internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        sharedPreferences = getSharedPreferences(MyPREFERENCES,MODE_PRIVATE);
        editor = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit();

        final EditText editTextEmail = (EditText) findViewById(R.id.email);

        final EditText editTextPassword = (EditText) findViewById(R.id.password);


        Button buttonLogIn = (Button) findViewById(R.id.loginbutton);
        buttonLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                email = editTextEmail.getText().toString().trim();
                password = editTextPassword.getText().toString().trim();
                //Toast.makeText(LoginActivity.this,email,Toast.LENGTH_SHORT).show();
                LoginProcess(email,password);
            }
        });
    }

    private void LoginProcess(final String sEmail, final String sPassword) {
        ApiInterfaces apiInterface = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelLogin> call = apiInterface.MODEL_CALL(sEmail,sPassword);
        call.enqueue(new Callback<ModelLogin>() {
            @Override
            public void onResponse(Call<ModelLogin> call, Response<ModelLogin> response) {
                int code = response.code();
                ModelLogin modelLogin = response.body();
                if (code == 200) {
                    String statusLogin = modelLogin.getStatus();
                    String messageLogin = modelLogin.getMessage();
                    int idLogin = modelLogin.getResult().getBpUserId();
                    String deviceTokenLogin = modelLogin.getResult().getToken();
                    //Toast.makeText(LoginActivity.this, statusLogin, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(LoginActivity.this, messageLogin, Toast.LENGTH_SHORT).show();
                    if (statusLogin.equals("success")) {
                        //Toast.makeText(LoginActivity.this, String.valueOf(idLogin), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra("device_token_petugas",deviceTokenLogin);
                        editor.putBoolean("isLogin",true);
                        editor.putString("token",deviceTokenLogin);
                        editor.apply();
                        startActivity(intent);
                        finish();
                    }
                    else if(statusLogin.equals("error")){
                        Toast.makeText(LoginActivity.this, "Your email or password incorrect!", Toast.LENGTH_SHORT).show();
                    }
                } else if(code == 401) {
                    Toast.makeText(LoginActivity.this, "Your email or password incorrect!", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(LoginActivity.this, statusLogin, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(LoginActivity.this, messageLogin, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ModelLogin> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Can't connect to server", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
