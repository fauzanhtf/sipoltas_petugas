package com.example.sipoltas_petugas;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by Fauzan on 23/08/2017.
 */

public class Connection {

    public static final boolean isConnected(Context context){
        boolean status;

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        status = networkInfo!=null && networkInfo.isConnectedOrConnecting();

        Toast.makeText(context,String.valueOf(status),Toast.LENGTH_SHORT).show();

        return status;
    }
}
