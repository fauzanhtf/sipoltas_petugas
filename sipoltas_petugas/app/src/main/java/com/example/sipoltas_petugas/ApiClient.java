package com.example.sipoltas_petugas;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Fauzan on 26/07/2017.
 */

public class ApiClient {
    public static final String BASE_URL = BuildConfig.ENV ;
    //http://192.168.43.51/panicbutton/lumen/public/v1/user/
    //http://sipoltas.isocorp.co.id/api/
    public static Retrofit retrofit = null;

    public static Retrofit getHttpClient(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient httpClient = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient)
                .build();

        return  retrofit;
    }
}
