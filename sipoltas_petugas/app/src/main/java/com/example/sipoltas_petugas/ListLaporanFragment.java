package com.example.sipoltas_petugas;


import android.content.SharedPreferences;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ListLaporanFragment extends Fragment {


    public ListLaporanFragment() {
        // Required empty public constructor
    }

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static final String MyPREFERENCES = "MyPrefs";

    int n=0;

    ListView listViewLaporan;

    String baseUrl = ApiClient.BASE_URL;
    String urlRequestGet;
    String device_token_id;

    int[] idLaporan;
    String[] keteranganLaporan;
    String[] waktuLaporan;
    String[] alamatLaporan;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_laporan, container, false);
        // Inflate the layout for this fragment

        Boolean statusInternet = Connection.isConnected(getActivity());

        if(!statusInternet){
            Toast.makeText(getActivity(), "You need a internet connection", Toast.LENGTH_LONG).show();
            return view;
        }

        listViewLaporan = (ListView) view.findViewById(R.id.listLaporan);

//        Intent intent = getActivity().getIntent();
//        device_token_id = intent.getExtras().getString("device_token_petugas");
        device_token_id = getToken();

        urlRequestGet = baseUrl+"v1/report/officer?token="+device_token_id;

        getLaporan();

        return view;
    }

    public String getToken(){
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES,getActivity().MODE_PRIVATE);
        String token = sharedPreferences.getString("token","0");
        if (token.equals("0")){
            startActivity(new Intent(getActivity(),LoginActivity.class));
            getActivity().finish();
        }
        return token;
    }

    private void getLaporan(){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelListLaporanMasuk> call = apiInterfaces.MODEL_LIST_LAPORAN_MASUK_CALL(urlRequestGet);
        call.enqueue(new Callback<ModelListLaporanMasuk>() {
            @Override
            public void onResponse(Call<ModelListLaporanMasuk> call, Response<ModelListLaporanMasuk> response) {
                int code = response.code();
                ModelListLaporanMasuk modelListLaporanMasuk = response.body();
                if(code == 200){
                    String statusGet = modelListLaporanMasuk.getStatus();
                    if(statusGet.equals("success")){
                        int nList = modelListLaporanMasuk.getResult().size();
                        keteranganLaporan = new String[nList];
                        idLaporan = new int[nList];
                        waktuLaporan = new String[nList];
                        alamatLaporan = new String[nList];
                        for(int i = nList-1; i >= 0; i--){
                            int id = modelListLaporanMasuk.getResult().get(i).getId();
                            String keterangan = modelListLaporanMasuk.getResult().get(i).getStatus().getName();
                            String waktu = modelListLaporanMasuk.getResult().get(i).getCreatedAt();
                            String alamat = modelListLaporanMasuk.getResult().get(i).getAddress();

                            idLaporan[n] = id;
                            keteranganLaporan[n] = keterangan ;
                            waktuLaporan[n] = waktu;
                            alamatLaporan[n] = alamat;
                            n++;
                        }
                        getListAdapter();
                    } else {

                    }
                } else if(code == 401){

                }
            }

            @Override
            public void onFailure(Call<ModelListLaporanMasuk> call, Throwable t) {

            }
        });
    }

    private void getListAdapter(){
        CustomListAdapter customListAdapter = new CustomListAdapter(getActivity(),keteranganLaporan,waktuLaporan,alamatLaporan,idLaporan);
        listViewLaporan.setAdapter(customListAdapter);
        listViewLaporan.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Fragment fragment = new DetailLaporanMasukFragment();
                Bundle bundle = new Bundle();
                bundle.putString("idLaporan", String.valueOf(idLaporan[position]));
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_view,fragment);
                fragmentTransaction.commit();

            }
        });
    }

}
