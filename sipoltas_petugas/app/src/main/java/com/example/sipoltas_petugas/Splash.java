package com.example.sipoltas_petugas;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class Splash extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    Boolean is_login = false;

    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Boolean statusInternet = Connection.isConnected(this);

        if(!statusInternet){
            Toast.makeText(this, "You need a internet connection", Toast.LENGTH_LONG).show();
            return;
        }

        sharedPreferences = getSharedPreferences(MyPREFERENCES,MODE_PRIVATE);
        is_login = sharedPreferences.getBoolean("isLogin",false);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(is_login){
                    Intent intent = new Intent(Splash.this, MainActivity.class);
                    intent.putExtra("From","splash");
                    startActivity(intent);
                    finish();
                }else {
                    startActivity(new Intent(Splash.this, LoginActivity.class));
                    finish();
                }
            }
        },1000);
    }
}
