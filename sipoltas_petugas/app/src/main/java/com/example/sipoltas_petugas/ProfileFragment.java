package com.example.sipoltas_petugas;


import android.content.SharedPreferences;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {


    public ProfileFragment() {
        // Required empty public constructor
    }

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static final String MyPREFERENCES = "MyPrefs";

    TextView textNama, textAddress, textProvinsi, textKota, textKecamatan, textEmail;
    String urlRequest;

    String baseUrl = ApiClient.BASE_URL;
    String device_token_id;

    Fragment fragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile,container,false);

        Boolean statusInternet = Connection.isConnected(getActivity());

        if(!statusInternet){
            Toast.makeText(getActivity(), "You need a internet connection", Toast.LENGTH_LONG).show();
            return view;
        }

//        Intent intent = getActivity().getIntent();
//        final String device_token_id = intent.getExtras().getString("device_token_petugas");
        device_token_id = getToken();


        textNama = (TextView) view.findViewById(R.id.name);
        textAddress = (TextView) view.findViewById(R.id.address);
        textProvinsi = (TextView) view.findViewById(R.id.provinsi);
        textKota = (TextView) view.findViewById(R.id.kota);
        textKecamatan = (TextView) view.findViewById(R.id.kecamatan);
        textEmail = (TextView) view.findViewById(R.id.email);

        Button buttonEdit = (Button) view.findViewById(R.id.buttonedit);
        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fragment = new EditProfileFragment();
                Bundle bundle = new Bundle();
                bundle.putString("device_token_petugas",device_token_id);
                fragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_view,  fragment);
                fragmentTransaction.commit();

//                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
//                intent.putExtra("device_token_petugas",device_token_id);
//                startActivity(intent);
            }
        });

        urlRequest = baseUrl+"v1/user/get?token="+device_token_id;
        //Toast.makeText(this,urlRequest,Toast.LENGTH_SHORT).show();

        getProfile(urlRequest);

        return view;
    }

    public String getToken(){
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES,getActivity().MODE_PRIVATE);
        String token = sharedPreferences.getString("token","0");
        if (token.equals("0")){
            startActivity(new Intent(getActivity(),LoginActivity.class));
            getActivity().finish();
        }
        return token;
    }

    private void getProfile(final String urlRequest){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelProfile> call = apiInterfaces.MODEL_CALL(urlRequest);
        call.enqueue(new Callback<ModelProfile>() {
            @Override
            public void onResponse(Call<ModelProfile> call, Response<ModelProfile> response) {
                int code = response.code();
                ModelProfile modelProfile = response.body();
                if(code==200){
                    String namaPetugas = modelProfile.getResult().getFullName();
                    String address = (String) modelProfile.getResult().getAddress();
                    String provinsi = modelProfile.getResult().getProvinsi().getName();
                    String kota = modelProfile.getResult().getKota().getName();
                    String kecamatan = modelProfile.getResult().getKecamatan().getName();
                    String email = modelProfile.getResult().getEmail();

                    //Toast.makeText(ProfileActivity.this,"Berhasil",Toast.LENGTH_SHORT).show();

                    textNama.setText(namaPetugas);
                    textAddress.setText(address);
                    textProvinsi.setText(provinsi);
                    textKota.setText(kota);
                    textKecamatan.setText(kecamatan);
                    textEmail.setText(email);
                } else {
                    Toast.makeText(getActivity(),"Gagal",Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(),LoginActivity.class));
                }
            }

            @Override
            public void onFailure(Call<ModelProfile> call, Throwable t) {
                Toast.makeText(getActivity(),"Gagal",Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getActivity(),LoginActivity.class));
            }
        });
    }

}
