package com.example.sipoltas_petugas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Fauzan on 14/08/2017.
 */

public class ResultListLaporanMasuk {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("bp_user_id")
    @Expose
    private Integer bpUserId;
    @SerializedName("officer_id")
    @Expose
    private Integer officerId;
    @SerializedName("attachment_id")
    @Expose
    private Integer attachmentId;
    @SerializedName("status_id")
    @Expose
    private Integer statusId;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("date_incident")
    @Expose
    private Object dateIncident;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("latitude")
    @Expose
    private Object latitude;
    @SerializedName("longitude")
    @Expose
    private String longitude;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("status")
    @Expose
    private StatusListLaporanMasuk status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBpUserId() {
        return bpUserId;
    }

    public void setBpUserId(Integer bpUserId) {
        this.bpUserId = bpUserId;
    }

    public Integer getOfficerId() {
        return officerId;
    }

    public void setOfficerId(Integer officerId) {
        this.officerId = officerId;
    }

    public Integer getAttachmentId() {
        return attachmentId;
    }

    public void setAttachmentId(Integer attachmentId) {
        this.attachmentId = attachmentId;
    }

    public Integer getStatusId() {
        return statusId;
    }

    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object getDateIncident() {
        return dateIncident;
    }

    public void setDateIncident(Object dateIncident) {
        this.dateIncident = dateIncident;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Object getLatitude() {
        return latitude;
    }

    public void setLatitude(Object latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public StatusListLaporanMasuk getStatus() {
        return status;
    }

    public void setStatus(StatusListLaporanMasuk status) {
        this.status = status;
    }
}
