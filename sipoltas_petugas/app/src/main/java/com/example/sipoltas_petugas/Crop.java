package com.example.sipoltas_petugas;

import android.content.Intent;
import android.graphics.drawable.Drawable;

/**
 * Created by Fauzan on 21/08/2017.
 */

public class Crop {
    public CharSequence title;
    public Drawable icon;
    public Intent appIntent;
}
