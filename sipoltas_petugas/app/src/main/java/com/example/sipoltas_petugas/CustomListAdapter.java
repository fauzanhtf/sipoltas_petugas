package com.example.sipoltas_petugas;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Fauzan on 14/08/2017.
 */

public class CustomListAdapter extends BaseAdapter {

    Context context;
    String[] keteranganLaporan;
    String[] waktuLaporan;
    String[] lokasiLaporan;
    int[] fotoLaporan;
    LayoutInflater layoutInflater;

    public CustomListAdapter(Context appcontext, String[] keterangan, String[] waktu, String[] lokasi, int[] foto){
        this.context = appcontext;
        this.keteranganLaporan = keterangan;
        this.waktuLaporan = waktu;
        this.lokasiLaporan = lokasi;
        //this.fotoLaporan = foto;
        layoutInflater = LayoutInflater.from(appcontext);
    }

    @Override
    public int getCount() {
        return keteranganLaporan.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.listview_laporan_masuk,parent,false);
        TextView keteranganLM = (TextView) convertView.findViewById(R.id.keteranganLaporan);
        TextView waktuLM = (TextView) convertView.findViewById(R.id.waktuLaporan);
        TextView lokasiLM = (TextView) convertView.findViewById(R.id.lokasiLaporan);
        ImageView fotoLM = (ImageView) convertView.findViewById(R.id.imageLaporan);

        keteranganLM.setText(keteranganLaporan[position]);
        waktuLM.setText(waktuLaporan[position]);
        lokasiLM.setText(lokasiLaporan[position]);
        //fotoLM.setImageResource(fotoLaporan.get(position));
        return convertView;
    }
}
