package com.example.sipoltas_petugas;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Fauzan on 21/08/2017.
 */

public class CropAdapter extends ArrayAdapter<Crop> {

    private ArrayList<Crop> mCrop;
    private LayoutInflater inflater;

    public CropAdapter(Context context, ArrayList<Crop> crop){
        super(context, R.layout.crop_selector,crop);

        mCrop = crop;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.crop_selector, null);
        }

        Crop item = mCrop.get(position);

        if(item!=null){
            ((ImageView) convertView.findViewById(R.id.iv_icon)).setImageDrawable(item.icon);
            ((TextView) convertView.findViewById(R.id.tv_name)).setText(item.title);

            return convertView;
        }

        return null;
    }
}
