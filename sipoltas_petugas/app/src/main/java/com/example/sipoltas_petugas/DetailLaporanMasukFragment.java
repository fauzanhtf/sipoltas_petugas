package com.example.sipoltas_petugas;


import android.content.SharedPreferences;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LOCATION_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetailLaporanMasukFragment extends Fragment implements OnMapReadyCallback,LocationListener {


    public DetailLaporanMasukFragment() {
        // Required empty public constructor
    }

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static final String MyPREFERENCES = "MyPrefs";

    private GoogleMap mMap;
    MapView mMapView;

    LocationManager locationManager;
    Location location;

    LatLng latLng;

    String device_token_id,idLaporan;

    TextView NamaPelapor,NoTelpPelapor,KeteranganKecelakaan,WaktuPelaporan;
    TextView addressPosition;

    private static final long MIN_DISTANCE_FOR_UPDATE = 10;
    private static final long MIN_TIME_FOR_UPDATE = 1000 * 60;
    private static final String TAG="Location Address";

    String urlRequestGet;

    String baseUrl = ApiClient.BASE_URL;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_laporan_masuk, container, false);

        Boolean statusInternet = Connection.isConnected(getActivity());

        if(!statusInternet){
            Toast.makeText(getActivity(), "You need a internet connection", Toast.LENGTH_LONG).show();
            return view;
        }

        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
        mMapView.getMapAsync(this);

//        Intent intent = getActivity().getIntent();
//        device_token_id = intent.getExtras().getString("device_token_petugas");
//        idLaporan = intent.getExtras().getString("IdLaporan");

        device_token_id = getToken();

        Bundle bundle = this.getArguments();
        if(bundle!=null){
            idLaporan = bundle.getString("idLaporan");
        }

        NamaPelapor = (TextView) view.findViewById(R.id.namaPelapor);
        NoTelpPelapor = (TextView) view.findViewById(R.id.noTelpPelapor);
        KeteranganKecelakaan = (TextView) view.findViewById(R.id.keteranganKecelakaan);
        WaktuPelaporan = (TextView) view.findViewById(R.id.waktuPelaporan);
        addressPosition = (TextView) view.findViewById(R.id.lokasiKejadian);

        urlRequestGet = baseUrl+"v1/report/report/"+idLaporan+"?token="+device_token_id;

        getLaporan();

        Button terimaButton = (Button) view.findViewById(R.id.updateLaporanButton);
        terimaButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new PerbaruiLaporanFragment();
                Bundle bundleKirim = new Bundle();
                bundleKirim.putString("idLaporan",idLaporan);
                bundleKirim.putString("token",device_token_id);
                fragment.setArguments(bundleKirim);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.fragment_view,fragment);
                fragmentTransaction.commit();
            }
        });

        return view;
    }

    public String getToken(){
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES,getActivity().MODE_PRIVATE);
        String token = sharedPreferences.getString("token","0");
        if (token.equals("0")){
            startActivity(new Intent(getActivity(),LoginActivity.class));
            getActivity().finish();
        }
        return token;
    }

    private void getLaporan(){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelLaporanMasuk> call = apiInterfaces.MODEL_LAPORAN_MASUK_CALL(urlRequestGet);
        call.enqueue(new Callback<ModelLaporanMasuk>() {
            @Override
            public void onResponse(Call<ModelLaporanMasuk> call, Response<ModelLaporanMasuk> response) {
                int code = response.code();
                ModelLaporanMasuk modelLaporanMasuk = response.body();
                if(code==200){
                    String statusLaporanMasuk = modelLaporanMasuk.getStatus();
                    if(statusLaporanMasuk.equals("success")){
                        String namaPelapor = modelLaporanMasuk.getResult().getUser().getFullName();
                        String noTelpPelapor = String.valueOf(modelLaporanMasuk.getResult().getUser().getPhoneNumber());
                        String keteranganLaporan = modelLaporanMasuk.getResult().getStatus().getName();
                        String waktuPelaporan = modelLaporanMasuk.getResult().getCreatedAt();
                        double latitudeLaporan = Double.parseDouble(modelLaporanMasuk.getResult().getLatitude());
                        double longitudeLaporan = Double.parseDouble(modelLaporanMasuk.getResult().getLongitude());

                        LatLng latLngLaporan = new LatLng(latitudeLaporan,longitudeLaporan);

                        mMap.addMarker(new MarkerOptions().position(latLngLaporan).title(getAddressFromPosition(latitudeLaporan,longitudeLaporan)));
                        CameraPosition cameraPosition = new CameraPosition.Builder()
                                .target(latLngLaporan)
                                .zoom(15)
                                .bearing(0)
                                .tilt(0)
                                .build();
                        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                        NamaPelapor.setText(namaPelapor);
                        NoTelpPelapor.setText(noTelpPelapor);
                        KeteranganKecelakaan.setText(keteranganLaporan);
                        WaktuPelaporan.setText(waktuPelaporan);
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelLaporanMasuk> call, Throwable t) {

            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(false);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
        location = locationManager.getLastKnownLocation(bestProvider);
        locationManager.requestLocationUpdates(bestProvider, MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);
        if (location != null) {
            onLocationChanged(location);
            setLocation();
        } else {
            Toast.makeText(getActivity(),"Your GPS is not available!",Toast.LENGTH_SHORT).show();
        }
    }

    private void setLocation(){
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        latLng =  new LatLng(latitude,longitude);
        mMap.clear();
        mMap.addMarker(new MarkerOptions().position(latLng).title(getAddressFromPosition(latitude,longitude)));
//        CameraPosition cameraPosition = new CameraPosition.Builder()
//                .target(latLng)
//                .zoom(15)
//                .bearing(0)
//                .tilt(0)
//                .build();
//        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private String getAddressFromPosition(double latitude, double longitude){
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(latitude,longitude,1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String address = addresses.get(0).getAddressLine(0);
//        String city = addresses.get(0).getLocality();
//        String state = addresses.get(0).getAdminArea();
//        String postalcode = addresses.get(0).getPostalCode();
//        String country = addresses.get(0).getCountryName();

//        String fulladdress = address;

        addressPosition.setText(address);

        return address;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
