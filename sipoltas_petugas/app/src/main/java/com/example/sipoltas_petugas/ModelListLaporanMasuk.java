package com.example.sipoltas_petugas;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Fauzan on 14/08/2017.
 */

public class ModelListLaporanMasuk {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code_status")
    @Expose
    private String codeStatus;
    @SerializedName("result")
    @Expose
    private List<ResultListLaporanMasuk> result = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCodeStatus() {
        return codeStatus;
    }

    public void setCodeStatus(String codeStatus) {
        this.codeStatus = codeStatus;
    }

    public List<ResultListLaporanMasuk> getResult() {
        return result;
    }

    public void setResult(List<ResultListLaporanMasuk> result) {
        this.result = result;
    }
}
