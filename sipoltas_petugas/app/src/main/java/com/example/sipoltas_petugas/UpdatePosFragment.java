package com.example.sipoltas_petugas;


import android.*;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LOCATION_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class UpdatePosFragment extends Fragment implements OnMapReadyCallback, LocationListener {


    public UpdatePosFragment() {
        // Required empty public constructor
    }

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public static final String MyPREFERENCES = "MyPrefs";

    private GoogleMap mMap;
    MapView mMapView;

    LocationManager locationManager;
    Location location;

    LatLng latLng;

    TextView banyakPetugas;

    String urlRequestGet,urlRequestGetBanyakPetugas,urlRequestPost;

    Spinner pilihanPos;
    TextView addressPosition;
    Button updatePosition;

    String baseUrl = ApiClient.BASE_URL;

    String device_token_id;

    List<Integer> idPos = new ArrayList<>();
    List<String> namaPos = new ArrayList<>();
    List<Double> latitudepos = new ArrayList<>();
    List<Double> longitudepos = new ArrayList<>();
    List<String> alamatPos = new ArrayList<>();

    private static final long MIN_DISTANCE_FOR_UPDATE = 10;
    private static final long MIN_TIME_FOR_UPDATE = 1000 * 60;
    private static final String TAG="Location Address";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_update_pos, container, false);

        Boolean statusInternet = Connection.isConnected(getActivity());

        if(!statusInternet){
            Toast.makeText(getActivity(), "You need a internet connection", Toast.LENGTH_LONG).show();
            return view;
        }

        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
        mMapView.getMapAsync(this);


//        Intent intent = getActivity().getIntent();
//        device_token_id = intent.getExtras().getString("device_token_petugas");
        device_token_id = getToken();

        urlRequestGet = baseUrl+"v1/office";
        urlRequestPost = baseUrl+"v1/user/location/join?token="+device_token_id;

        addressPosition = (TextView) view.findViewById(R.id.address);
        updatePosition = (Button) view.findViewById(R.id.joinPosButton);
        pilihanPos = (Spinner) view.findViewById(R.id.spinnerPilihanPos);
        banyakPetugas = (TextView) view.findViewById(R.id.banyakJumlahPetugas);
        idPos.add(0,0);
        namaPos.add(0,"--Pilih Pos Penjagaan--");
        latitudepos.add(0,0.0);
        longitudepos.add(0,0.0);
        alamatPos.add(0,"-");
        getPos();

        return view;
    }

    public String getToken(){
        sharedPreferences = getActivity().getSharedPreferences(MyPREFERENCES,getActivity().MODE_PRIVATE);
        String token = sharedPreferences.getString("token","0");
        if (token.equals("0")){
            startActivity(new Intent(getActivity(),LoginActivity.class));
            getActivity().finish();
        }
        return token;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        locationManager = (LocationManager) getActivity().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(false);
        Criteria criteria = new Criteria();
        String bestProvider = locationManager.getBestProvider(criteria, true);
        location = locationManager.getLastKnownLocation(bestProvider);
        locationManager.requestLocationUpdates(bestProvider, MIN_TIME_FOR_UPDATE, MIN_DISTANCE_FOR_UPDATE, this);
        if (location != null) {
            onLocationChanged(location);
            setLocation();
        } else {
            Toast.makeText(getActivity(),"Your GPS is not available!",Toast.LENGTH_SHORT).show();
        }
    }

    private void getPos(){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelUpdatePos> call = apiInterfaces.MODEL_UPDATE_POS_CALL(urlRequestGet);
        call.enqueue(new Callback<ModelUpdatePos>() {
            @Override
            public void onResponse(Call<ModelUpdatePos> call, Response<ModelUpdatePos> response) {
                int code = response.code();
                ModelUpdatePos modelUpdatePos = response.body();
                if(code==200){
                    String statusGetPos = modelUpdatePos.getStatus();
                    if(statusGetPos.equals("success")){
                        List<ResultUpdatePos> result = modelUpdatePos.getResult();
                        int panjangresult = result.size();
                        for(int i=0;i<panjangresult;i++){
                            int id = modelUpdatePos.getResult().get(i).getId();
                            String nama = modelUpdatePos.getResult().get(i).getName();
                            double lat = Double.parseDouble(modelUpdatePos.getResult().get(i).getLatitude());
                            double log = Double.parseDouble(modelUpdatePos.getResult().get(i).getLongitude());
                            String alamat = modelUpdatePos.getResult().get(i).getAddress();
                            int posisi = i+1;
                            idPos.add(posisi,id);
                            namaPos.add(posisi,nama);
                            latitudepos.add(posisi,lat);
                            longitudepos.add(posisi,log);
                            alamatPos.add(posisi,alamat);

                            drawMarkerPos(lat,log,nama);
                            //Toast.makeText(UpdatePosActivity.this, namaPos.get(i),Toast.LENGTH_SHORT).show();
                        }
                        setSpinnerPilihan();
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelUpdatePos> call, Throwable t) {

            }
        });
    }

    private void postUpdatePos(final double latitude, final double longitude, final int idPos){
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelUpdatePosition> call = apiInterfaces.MODEL_UPDATE_POS_CALL(urlRequestPost,String.valueOf(latitude),String.valueOf(longitude),idPos);
        call.enqueue(new Callback<ModelUpdatePosition>() {
            @Override
            public void onResponse(Call<ModelUpdatePosition> call, Response<ModelUpdatePosition> response) {
                int code = response.code();
                ModelUpdatePosition modelUpdatePosition = response.body();
                if(code == 201){
                    String statusPost = modelUpdatePosition.getStatus();
                    if(statusPost.equals("success")){
                        getBanyakPetugas(idPos);
                        Toast.makeText(getActivity(),"Posisi Anda berhasil di update!",Toast.LENGTH_SHORT).show();
                    } else if(statusPost.equals("error")){
                        Toast.makeText(getActivity(),"Terjadi kesalahan saat mengupdate posisi Anda!",Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelUpdatePosition> call, Throwable t) {

            }
        });
    }

    private void drawMarkerPos(double latitude, double longitude, String nama){
        LatLng latLng = new LatLng(latitude,longitude);
        mMap.addMarker(new MarkerOptions().position(latLng).title(nama).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW)));
    }

    private void setSpinnerPilihan(){
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(),R.layout.support_simple_spinner_dropdown_item,namaPos);
        pilihanPos.setAdapter(arrayAdapter);
        pilihanPos.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, final int position, long id) {

                addressPosition.setText(alamatPos.get(position));
                getBanyakPetugas(idPos.get(position));

                updatePosition.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        double latitude = location.getLatitude();
                        double longitude = location.getLongitude();
                        postUpdatePos(latitude,longitude,idPos.get(position));
                    }
                });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getBanyakPetugas(final int index){
        urlRequestGetBanyakPetugas = baseUrl+"v1/office/count/"+index;
        ApiInterfaces apiInterfaces = ApiClient.getHttpClient().create(ApiInterfaces.class);
        Call<ModelJumlahPetugas> call = apiInterfaces.MODEL_JUMLAH_PETUGAS_CALL(urlRequestGetBanyakPetugas);
        call.enqueue(new Callback<ModelJumlahPetugas>() {
            @Override
            public void onResponse(Call<ModelJumlahPetugas> call, Response<ModelJumlahPetugas> response) {
                int code = response.code();
                ModelJumlahPetugas modelJumlahPetugas = response.body();
                if(code==200){
                    String statusJumlahPetugas = modelJumlahPetugas.getStatus();
                    if(statusJumlahPetugas.equals("success")){
                        int jumlahPetugas = modelJumlahPetugas.getResult();
                        banyakPetugas.setText(String.valueOf(jumlahPetugas));
                    }
                }
            }

            @Override
            public void onFailure(Call<ModelJumlahPetugas> call, Throwable t) {

            }
        });
    }

    Marker markerPosition;

    private void setLocation(){
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        latLng =  new LatLng(latitude,longitude);
        mMap.clear();
        markerPosition = mMap.addMarker(new MarkerOptions().position(latLng).title(getAddressFromPosition(latitude,longitude)));
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(latLng)
                .zoom(15)
                .bearing(0)
                .tilt(0)
                .build();
//        Circle circle = mMap.addCircle(new CircleOptions()
//                .center(latLng)
//                .radius(1000)
//                .strokeColor(Color.YELLOW)
//                .fillColor(0x22FFFF00)
//                .strokeWidth(5));
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        //postPosition(latitude,longitude);
    }

    private String getAddressFromPosition(double latitude, double longitude){
        Geocoder geocoder = new Geocoder(getActivity(), Locale.getDefault());
        List<Address> addresses = null;

        try {
            addresses = geocoder.getFromLocation(latitude,longitude,1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String address = addresses.get(0).getAddressLine(0);
//        String city = addresses.get(0).getLocality();
//        String state = addresses.get(0).getAdminArea();
//        String postalcode = addresses.get(0).getPostalCode();
//        String country = addresses.get(0).getCountryName();

//        String fulladdress = address +",\n"+ city+",\n"+state+",\n"+postalcode;

        return address;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
